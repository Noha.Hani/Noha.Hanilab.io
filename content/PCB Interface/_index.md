---
title: PCB Interface 
weight: -20
---


Using the programmer , We uploaded a code to our board allowing interface using Processing 3 .

<img src="/media/interface/allow.png"  height="600">

Openned the Code on Processing 3 and complete missing lines .

<img src="/media/interface/complete.png"  height="600">

Run the code and test 

<img src="/media/interface/run.png"  height="600">

Turned out that some leds responded to wrong bottoms , so I had to switch their commanding lines and now it's working properly !!


<video width="600" controls>
  <source src="/media/interface/test.mp4" type="video/mp4">
</video>

<p>
	It's another led festival !
</p>
