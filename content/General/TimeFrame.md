---
title: Journey's Time Frame
weight: -20
---

I love to keep track of my steps date-related , So I'll be updating here when every step was taken.
It helps me consider how long I took to aquire a skill or fully understand a concept.

<!-- spellchecker-disable -->

{{< toc >}}

<!-- spellchecker-enable -->


## Website Building
 2nd Jan        .. Online session where we were introduced to all the softwares we will need to use.
 
 3rd & 4th Jan  .. I was figuring out how things work and worked on my website.
 
 5th Jan        .. I was making sure I understand what I did and why I did it.
 
 6th Jan        .. After getting everything checked, I submitted the task =) 
 
## PCB Fabrication
 
 7th Jan        .. We had an offline session on PCB fabrication.
 
 10th Jan       .. PCB milled my board, soldered components and installed first code.
 
## Interfce
 
 15th Jan       .. Worked on Processing 3 software to establish my interface with the board.