---
title: About Me !
weight: -20
---

## Who Am I ?
 I'm an Electroniccs & Communication Engineering student, Fablab specialist at FabLab Egypt and an Origamist.
 I am fascinated with the making movement and utilizing digital fabrication to turn ideas into reality making life easier .
 Call me a cat-lady as I adore these tiny cute creatures. 
 I love to read, listen to music (Good ones), travel around and try new stuff.

## Why Fab Academy ?
 Digital fabrication is a wide world with endless possibilities and the Fab Academy is a great push through it . 
 I want to be a better Fab Lab specialist as I intend to proceed in the digital fabrication career path .
 I started already encouraging people in my community to consider digital fabrication into their different professions and I want to be able to help them more .


<img src="/media/me.png" width="400" height="400">

